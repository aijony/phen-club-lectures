#!/bin/awk -f

NR > 2 {
	for(i=1;i<=NF;i++){
		sum[i] += $i;
		sum[i] /= NR;
	};
}
END {
	vol = sum[2] + sum[3] + sum[4];
	den = sum[1] / 1000 / (vol / 100);
	print "The density is ",den," kg/m^3";
}
